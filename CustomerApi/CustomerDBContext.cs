﻿using CustomerApi.Models;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi
{
    public class CustomerDBContext: DbContext
    {
        public CustomerDBContext(DbContextOptions<CustomerDBContext> options)
            : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
    }
}
