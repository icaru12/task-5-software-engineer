﻿using CustomerApi.Models;

namespace CustomerApi.Services
{
    public class TodoApiService
    {
        private readonly HttpClient _httpClient;

        public TodoApiService(HttpClient httpClient)
        {
            _httpClient = httpClient;

            _httpClient.BaseAddress = new Uri("https://jsonplaceholder.typicode.com/");
        }

        // Asynchronous external API call
        public async Task<IEnumerable<Todo>?> GetTodosAsync()
        {
            var todos = await _httpClient.GetFromJsonAsync<IEnumerable<Todo>>("todos");

            return todos;
        }

        // Synchronous external API call
        public Todo? GetTodo(int todoId)
        {
            var todo = _httpClient.GetFromJsonAsync<Todo>($"todos/{todoId}").Result;

            return todo;
        }
    }
}
