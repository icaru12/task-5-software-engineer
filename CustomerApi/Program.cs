using CustomerApi;
using CustomerApi.Services;
using Microsoft.EntityFrameworkCore;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.ClearProviders();
builder.Host.UseSerilog((context, services, configuration) =>
{
    configuration
        .WriteTo.File("logs/customerapi-log.txt")
        .WriteTo.Console();
});

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        policy =>
        {
            if (builder.Environment.IsDevelopment())
            {
                policy.AllowAnyOrigin();
            }
            else
            {
                policy.WithOrigins("http://localhost:8080");
            }
        });
});

// Add services to the container.
builder.Services.AddDbContext<CustomerDBContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("CustomersDb")));

builder.Services.AddHttpClient<TodoApiService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors();

app.UseAuthorization();

app.MapControllers();

app.Run();
