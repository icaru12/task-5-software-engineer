﻿using CustomerApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace CustomerApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodosController : ControllerBase
    {
        private readonly ILogger<TodosController> _logger;
        private readonly TodoApiService _todoApiService;

        public TodosController(ILogger<TodosController> logger, TodoApiService todoApiService)
        {
            _logger = logger;
            _todoApiService = todoApiService;
        }

        // GET: api/todos
        [HttpGet]
        public async Task<IActionResult> GetTodos()
        {
            try
            {
                var todos = await _todoApiService.GetTodosAsync();

                return Ok(todos);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());

                return Problem(ex.Message, statusCode: 500);
            }
        }

        // GET: api/todos/{id}
        [HttpGet("{id}")]
        public IActionResult GetTodo(int id)
        {
            try
            {
                var todo = _todoApiService.GetTodo(id);

                return Ok(todo);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());

                return Problem(ex.Message, statusCode: 500);
            }
        }
    }
}
